# Technical assessment - SQL 


## Analyzis of complexity

**Find author by name "Leo"**

The time complexity of this query is O(1), which means that the query will take a constant amount of time to execute, regardless of the number of authors or books in the database. This is because the query only needs to search for a single row in the authors table.

**Find books of author "Fitzgerald"**

The time complexity of this query is O(n), where n is the number of books in the database. This is because the query needs to search all of the rows in the books table to find the books that are authored by Fitzgerald.

**Find authors without books**

The time complexity of this query is O(m), where m is the number of authors in the database. This is because the query needs to search all of the rows in the authors table to find the authors that do not have any books.

**Count books per country**

The time complexity of this query is O(n), where n is the number of books in the database. This is because the query needs to iterate through all of the rows in the books table to count the number of books for each country.

