SELECT * FROM authors WHERE name = 'Leo';

SELECT * FROM books WHERE author_name = 'F. Scott. Fitzgerald';

SELECT * FROM authors WHERE name NOT IN (
    SELECT author_name FROM books
);

SELECT country, COUNT(*) AS num_books FROM books GROUP BY country;

SELECT author_name, AVG(pages) AS avg_length FROM books GROUP BY author_name;
